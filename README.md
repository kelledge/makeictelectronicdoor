![MakeICT-Logo-web.png](https://bitbucket.org/repo/BMjob4/images/2744186893-MakeICT-Logo-web.png)
## MakeICT/Bluebird Arthouse Electronic Door Entry Project

## Authors:
* Dominic Canare <dom@greenlightgo.org>
* Rye Kennedy
* John Harrison
* Christian Kindel
* Tom McGuire

* * *

This is a work in progress. For more information, please visit the [MakeICT Wiki](http://makeict.org/wiki/index.php/Electronic_Door_Entry).

* * *

## TODO
* Documentation
* (form) Edit user
* Log viewer
* Testing
